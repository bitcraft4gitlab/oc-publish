<?php namespace Bitcraft\Publish;

use Bitcraft\Novid20\Models\Page;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        Event::listen('versions.unpublish', function($id) {
            $page = Page::findOrFail($id);
            $page->handlePublishedPages();
        });
    }

    public function registerSchedule($schedule)
    {
        $schedule->command('queue:restart')->hourly();
        $schedule->command('queue:work --delay=300 --timeout=9999 --tries=254')
            ->runInBackground()->withoutOverlapping()->everyTenMinutes();
    }
}
