<?php namespace Bitcraft\Publish\Behaviors;

use Backend\Facades\BackendAuth;
use Bitcraft\Publish\Classes\Cloudfront;
use Bitcraft\Publish\Jobs\UploadPages;
use Bitcraft\Versions\Models\Version;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use RainLab\Translate\Models\Locale;

class PublishModelBehavior extends \October\Rain\Extension\ExtensionBase
{
    protected $parent;

    public function __construct($parent)
    {
        $this->parent = $parent;
    }

    public function handlePublishedPages()
    {
        $path = $this->parent->slug === '/' ? 'index' : $this->parent->slug;
        $this->removeFromS3($path);
        $locales = array_keys(Locale::listEnabled());
        foreach ($locales as $locale) {
            $this->removeFromS3("$locale$path");
        }
        Cloudfront::invalidate(config('app.cloudfront_id'));
    }

    public function removeFromS3($path) {
        Storage::disk($this->parent->platform->bucket)->delete($path);
    }

    public function renderPage()
    {
        $publish_at = post('publish_at');
        $date = empty($publish_at) ? Carbon::now() : Carbon::createFromTimeString($publish_at);
        Queue::later($date, UploadPages::class, ['id' => $this->parent->id, 'user' => BackendAuth::getUser()->login]);
    }
}
