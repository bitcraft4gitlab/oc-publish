<?php namespace Bitcraft\Publish\Models;

use Model;

/**
 * Model
 */
class Platform extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_publish_platforms';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'settings' => 'Bitcraft\Novid20\Models\Setting',
    ];
}
