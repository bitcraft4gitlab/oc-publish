<?php namespace Bitcraft\Publish\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftPublishPlatforms extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_publish_platforms', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('title');
            $table->string('domain')->nullable();
            $table->string('bucket')->nullable();
            $table->integer('settings_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bitcraft_publish_platforms');
    }
}
