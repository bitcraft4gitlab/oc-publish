<?php namespace Bitcraft\Publish\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftPublishPastQueueJobs extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_publish_past_queue_jobs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('type')->nullable();
            $table->string('user')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bitcraft_publish_past_queue_jobs');
    }
}
